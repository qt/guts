# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
"""Running functions from the command line"""
from __future__ import division

def numpy_version():
    import numpy.version as nv
    ver = nv.version
    if not nv.release:
        ver +=  '-non-release'
    return ver

def scipy_version():
    import scipy.version as nv
    ver = nv.version
    if not nv.release:
        ver +=  '-non-release'
    return ver

def randomize():
    """Seed numpy's random generator according to RNG_SEED env variable.

    If RNG_SEED is undefined or has the value "random", the seed is read from
    /dev/urandom.  Otherwise, the value of RNG_SEED (which may be the decimal
    representation of an 8-byte signed integer) is used as seed."""
    import os, struct
    from numpy import random
    try:
        seed = os.environ['RNG_SEED']
    except KeyError:
        seed = 'random'
    if seed == 'random':
        f = open('/dev/urandom')
        seed = struct.unpack('Q', f.read(8))[0]
        f.close()
    else:
        seed = int(seed)

    assert seed >= 0 and seed < 1<<64
    seed_lo = int((seed & 0xffffffff) - (1<<31))
    seed_hi = int((seed >> 32) - (1<<31))
    random.seed((seed_lo, seed_hi))

    return seed

def exec_argv(program_name, vars):
    import sys
    from version import version

    if len(sys.argv) == 1:
        help('__main__')
        return

    seed = randomize()
    print '#### guts %s %s, scipy %s, numpy %s' % \
        (program_name, version, scipy_version(), numpy_version())
    print "#### numpy random seed: %d" % seed
    for statement in sys.argv[1:]:
        print "## %s" % statement
        exec statement in vars
