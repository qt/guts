# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
"""Izak Snyman's network model, arXiv:0803.2197

Consider this as highly experimental.  This code was converted directly from the
C++ written by Jakub Tworzydlo.  The notation used here differs slightly from
the paper (t and tp are exchanged).
"""
from __future__ import division
import numpy as np, numpy.matlib as ml
from guts import sm

def smatrices_mass_slice(masspot, m=0, dx=1):
    """Return two scattering matrices for a slice of the model."""
    from math import cos, sin, pi

    # abbreviations
    m_over_2 = m / 2
    pi_over_4 = pi / 4

    n = masspot.shape[0]
    assert n >= 3
    assert masspot.shape[1] == 2
    vm0 = masspot[:, 0].reshape(n)
    vm1 = masspot[:, 1].reshape(n)

    # slice 1
    s1 = sm.SMatrix()
    s1.r, s1.t, s1.tp, s1.rp = (ml.zeros((n, n)), ml.zeros((n, n)),
                                ml.zeros((n, n)), ml.zeros((n, n)))

    # slice 2
    s2 = sm.SMatrix()
    s2.r, s2.t, s2.tp, s2.rp = (ml.zeros((n, n)), ml.zeros((n, n)),
                                ml.zeros((n, n)), ml.zeros((n, n)))

    ca, sa, cb, sb = np.empty(n), np.empty(n), np.empty(n), np.empty(n)

    for l in range(n):
        ca[l] = cos(pi_over_4 + vm0[l] / 2 + m_over_2)
        sa[l] = sin(pi_over_4 + vm0[l] / 2 + m_over_2)
        cb[l] = cos(pi_over_4 + vm0[l] / 2 + m_over_2)
        sb[l] = sin(pi_over_4 + vm0[l] / 2 + m_over_2)

    for l in range(n):
        pr = (l - 1) % n
        nx = (l + 1) % n
        s1.r[l, l]   =  ca[l]  * cb[l]
        s1.r[l, pr]  = -sa[pr] * sb[l]
        s1.t[l, l]   =  ca[pr] * sb[l]
        s1.t[l, nx]  =  sa[l]  * cb[l]
        s1.tp[l, l]  =  ca[l]  * sb[l]
        s1.tp[l, pr] =  sa[pr] * cb[l]
        s1.rp[l, l]  = -ca[pr] * cb[l]
        s1.rp[l, nx] =  sa[l]  * sb[l]

    for l in range(n):
        ca[l] = cos(pi_over_4 + vm0[l] / 4 + vm1[l] / 4 + m_over_2)
        sa[l] = sin(pi_over_4 + vm0[l] / 4 + vm1[l] / 4 + m_over_2)
        cb[l] = cos(pi_over_4 + vm0[l] / 4 + vm1[l] / 4 + m_over_2)
        sb[l] = sin(pi_over_4 + vm0[l] / 4 + vm1[l] / 4 + m_over_2)

    for l in range(n):
        pr = (l - 1) % n
        nx = (l + 1) % n
        s2.r[l, l]   =  ca[l]  * cb[l]
        s2.r[l, pr]  = -sa[pr] * sb[l]
        s2.t[l, l]   =  sa[l]  * cb[l]
        s2.t[l, pr]  =  ca[pr] * sb[l]
        s2.tp[l, l]  =  sa[l]  * cb[nx]
        s2.tp[pr, l] =  ca[l]  * sb[l]
        s2.rp[l, l]  = -ca[l]  * cb[nx]
        s2.rp[pr, l] =  sa[l]  * sb[l]

    return s1, s2

def smatrices_mass(masspot):
    """Generate the scattering matrices for a given mass potential."""
    length = masspot.shape[1]
    for x in range(length - 1):
        s1, s2 = smatrices_mass_slice(masspot[:, x:x+2])
        yield s1
        yield s2
