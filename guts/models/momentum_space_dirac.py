# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
"""Momentum space discretization of Dirac equation, generalization of
arXiv:0705:0886

More information to follow.
"""

from __future__ import division
import numpy as np, numpy.matlib as ml
from guts import sm

# TODO: generalize to twisted periodic boundary conditions, use sparse and band
# matrices where possible

def mmatrix_modes(n, inv_asp, e=0, ax=0, ay=0, mass=0, ret_s=False):
    """Return transfer matrix of the ballistic part of the slice.

    n --- number of positive momenta taken
    inv_asp --- inverse aspect ratio of the sample (length/width).
    e --- average scalar potential
    ax --- average vector potential in x direction
    ay --- average vector potential in y direction
    mass --- average mass.
    Energies are measured in units inverse slice thickness.
    Taking inv_asp = 0 gives a delta-function potential barrier.

    If ret_s=True, return a scattering matrix instead.
    """
    from numpy import cos, sin, sqrt, exp, pi
    assert n > 0

    if not ret_s:
        m = sm.MMatrix()
        q = np.arange(-n, n+1)
        q1 = ay + 2 * pi * q * inv_asp
        kx = sqrt(e**2 - q1**2 - mass**2 + 0j)
        coskx = cos(kx)
        sinkxkx = np.zeros((len(kx)), np.complex)
        sinkxkx = sin(kx)/kx
        sinkxkx[np.isnan(sinkxkx)] = 1
        m.m11 = np.mat(np.diag(exp(1j * ax) * (coskx + 1j * e * sinkxkx)))
        m.m12 = np.mat(np.diag(exp(1j * ax) * (mass + 1j * q1) * sinkxkx))
        m.m21 = np.mat(np.diag(exp(1j * ax) * (mass - 1j * q1) * sinkxkx))
        m.m22 = np.mat(np.diag(exp(1j * ax) * (coskx - 1j * e * sinkxkx)))
        return m
    else:
        s = sm.SMatrix()
        q = np.arange(-n, n+1)
        q1 = ay + 2 * pi * q * inv_asp
        kx = sqrt(e**2 - q1**2 - mass**2 + 0j)
        coskx = cos(kx)
        sinkxkx = np.zeros((len(kx)), np.complex)
        sinkxkx = sin(kx) / kx
        sinkxkx[np.isnan(sinkxkx)] = 1
        s.r = np.mat(np.diag((1j * q1 - mass) * sinkxkx /
                     (coskx - 1j * e * sinkxkx)))
        s.t = np.mat(np.diag(exp( 1j * ax) / (coskx - 1j * e * sinkxkx)))
        s.tp = np.mat(np.diag(exp(-1j * ax) / (coskx - 1j * e * sinkxkx)))
        s.rp = np.mat(np.diag((1j * q1 + mass) * sinkxkx /
                              (coskx - 1j * e * sinkxkx)))
        return s

def mmatrix_mixing(v, ax, ay, mass, ret_s=False):
    """Return the slice transfer matrix due to disordered potential.

    v --- positive fourier components of the scalar potential
    ax --- positive fourier components of the vector potential ax
    ay --- positive fourier components of the vector potential ay
    mass --- positive fourier components of the mass m.

    Importantly, each of these has to be in units of inverse slice thickness.

    Negative fourier components are calculated from positive ones.
    Zeroth fourier component should be supplied to mmatrix_modes.
    """
    n = v.shape[0]
    assert ax.shape[0] == n
    assert ay.shape[0] == n
    assert mass.shape[0] == n
    assert n % 2 == 0

    mat_v = ml.zeros((n + 1, n + 1), np.complex)
    mat_ax = ml.zeros((n + 1, n + 1), np.complex)
    mat_ay = ml.zeros((n + 1, n + 1), np.complex)
    mat_mass = ml.zeros((n + 1, n + 1), np.complex)

    # Generate matrices from fourier transforms.
    for q1 in range(0, n + 1):
        for q2 in range(q1 + 1, n + 1):
            mat_v[q1, q2] = v[q2 - q1 -1]
            mat_ax[q1, q2] = ax[q2 - q1 -1]
            mat_ay[q1, q2] = ay[q2 - q1 -1]
            mat_mass[q1, q2] = mass[q2 - q1 -1]
    mat_v += mat_v.H
    mat_ax += mat_ax.H
    mat_ay += mat_ay.H
    mat_mass += mat_mass.H
    n += 1

    # Calculate current-conserving transfer matrix.
    # TODO: optimize this
    m = ml.empty((2*n, 2*n), np.complex)
    m[: n, : n] = 1j * mat_v + 1j * mat_ax
    m[: n, n :] = mat_mass + 1j * mat_ay
    m[n :, : n] = mat_mass - 1j * mat_ay
    m[n :, n :] = -1j * mat_v + 1j * mat_ax
    m = (ml.identity(2*n) + m) * (ml.identity(2*n) - m).I
    m = sm.MMatrix.from_matrix(m)
    if not ret_s:
        return m
    else:
        return sm.SMatrix.from_m(m)

def mmatrices((w, l), inv_asp=1, v=None, ax=None, ay=None,
              mass=None, ret_s=True):
    """Generate the transfer matrices for a given system.

    First the dimensions of the system are set in the format (width, length),
    with width the number of transverse modes with positive momenta, and length
    the number of slices in the system.  The system is specified by the 2d
    arrays v, ax, ay, and mass which have to have length equal to the number of
    slices in the system. If the width of these arrays is too small, zeros are
    added.

    The transfer matrices are generated from right to left to allow combining
    them with guts.sm.fast_combine.
    """

    # Initialization of values and formatting
    if v == None:
        v = np.zeros((0, l))
    if ax == None:
        ax = np.zeros((0, l))
    if ay == None:
        ay = np.zeros((0, l))
    if mass == None:
        mass = np.zeros((0, l))
    assert l == v.shape[1] == ax.shape[1] == ay.shape[1] == mass.shape[1]

    v = np.vstack((v, np.zeros((2*w + 1 - v.shape[0], l)))) / l
    ax = np.vstack((ax, np.zeros((2*w + 1 - ax.shape[0], l)))) / l
    ay = np.vstack((ay, np.zeros((2*w + 1 - ay.shape[0], l)))) / l
    mass = np.vstack((mass, np.zeros((2*w + 1 - mass.shape[0], l)))) / l
    inv_asp /= l

    # Calculating transfer matrices slice by slice
    for x in reversed(range(l)):
        m1 = mmatrix_modes(w, inv_asp, v[0, x], ax[0, x],
                           ay[0, x], mass[0, x], ret_s)
        m2 = mmatrix_mixing(v[1 : , x], ax[1 : , x],
                            ay[1 : , x], mass[1 : , x], ret_s)
        yield m1
        yield m2
