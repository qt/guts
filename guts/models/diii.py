# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
"""Benjamin Beri's DIII network model"""
from __future__ import division
import numpy as np, numpy.matlib as ml
from guts import sm

# TODO: verify that eta contains only 1 and -1

def smatrix_s_slice(alpha, theta, eta):
    """Return the scattering matrix of a S-slice."""
    from math import cos, sin

    n = len(alpha)
    assert len(theta) == n
    assert len(eta) == n
    n2 = 2 * n
    n4 = 4 * n

    s_mat = ml.zeros((n4, n4))
    sr = s_mat[0 : n2, 0 : n2]
    stp = s_mat[0 : n2, n2 : n4]
    st = s_mat[n2 : n4, 0 : n2]
    srp = s_mat[n2 : n4, n2 : n4]

    for i in range(n):
        r, t = cos(alpha[i]), sin(alpha[i])
        c, s = cos(theta[i]), sin(theta[i])
        e = eta[i]
        j = 2 * i
        k = j + 1

        sr[j, k] = r
        sr[k, j] = -r

        stp[j, j] = t * c
        stp[j, k] = -t * s
        stp[k, j] = t * e * s
        stp[k, k] = t * e * c

        st[j, j] = -t * c
        st[j, k] = -t * e * s
        st[k, j] = t * s
        st[k, k] = -t * e * c

        srp[j, k] = -e * r
        srp[k, j] = e * r

    return sm.SMatrix.from_matrix(s_mat)

def smatrix_sp_slice(alpha, theta, eta):
    """Return the scattering matrix of a S'-slice."""
    from math import cos, sin

    n = len(alpha)
    assert len(theta) == n
    assert len(eta) == n
    n2 = 2 * n
    n4 = 4 * n

    s_mat = ml.zeros((n4, n4))
    sr = s_mat[0 : n2, 0 : n2]
    stp = s_mat[0 : n2, n2 : n4]
    st = s_mat[n2 : n4, 0 : n2]
    srp = s_mat[n2 : n4, n2 : n4]

    for i in range(n):
        r, t = cos(alpha[i]), sin(alpha[i])
        c, s = cos(theta[i]), sin(theta[i])
        e = -eta[i]
        j = (2 * i - 1) % n2
        k = 2 * i

        sr[j, k] = t * e * c
        sr[k, j] = -t * e * c

        stp[j, j] = -r
        stp[j, k] = t * e * s
        stp[k, j] = t * s
        stp[k, k] = e * r

        st[j, j] = r
        st[j, k] = -t * s
        st[k, j] = -t * e * s
        st[k, k] = -e * r

        srp[j, k] = t * c
        srp[k, j] = -t * c

    return sm.SMatrix.from_matrix(s_mat)

def smatrices(alpha, theta, eta, leftmost_slice_is_s=True):
    """Generate the scattering matrices for a given system.

    The system is specified by the 2d arrays alpha, theta and eta which have to
    be the same size as the system.  For each column of the system, the
    corresponding columns of the parameter arrays are passed on alternately to
    smatrix_s_slice or smatrix_sp_slice.

    The scattering matrices are generated from right to left to allow combining
    them with guts.sm.fast_combine.
    """
    assert alpha.shape == theta.shape
    assert alpha.shape == eta.shape
    width = alpha.shape[0]
    length = alpha.shape[1]
    for x in reversed(range(length)):
        a = alpha[:, x].reshape(width)
        t = theta[:, x].reshape(width)
        e = eta[:, x].reshape(width)
        if bool(x % 2) != bool(leftmost_slice_is_s):
            yield smatrix_s_slice(a, t, e)
        else:
            yield smatrix_sp_slice(a, t, e)
