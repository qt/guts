# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
from __future__ import division
import numpy as np, numpy.matlib as ml
from numpy import array as ar

from guts.models import momentum_space_dirac as msd
from guts import misc, sm

# TODO: add more tests

def test_mmatrix_modes():
    m = msd.mmatrix_modes(3, 0.2, 1.2, .18, .12, .11)
    s = msd.mmatrix_modes(3, 0.2, 1.2, .18, .12, .11, True)
    m1 = sm.MMatrix.from_s(s)
    assert misc.how_different(m.to_matrix(), m1.to_matrix()) < 1e-15
    assert m.how_not_conserving() < 1e-5, m.how_not_conserving()
    m = msd.mmatrix_modes(2, 1)
    assert m.how_not_conserving() < 1e-5, m.how_not_conserving()

def test_mmatrix_mixing():
    m = msd.mmatrix_mixing(ar([.1, .2]), ar([.11, .14]),
                           ar([.22, .34]), ar([.18 + .20j, -.12]))
    s = msd.mmatrix_mixing(ar([.1, .2]), ar([.11, .14]),
                           ar([.22, .34]), ar([.18 + .20j, -.12]), True)
    m1 = sm.MMatrix.from_s(s)
    assert abs(m.to_matrix() - m1.to_matrix()).max() < 1e-14
    assert m.how_not_conserving() < 1e-7, m.how_not_conserving()

def test_mmatrices():
    from operator import mul
    mats = msd.mmatrices((1, 2),
                         v=ar([[.1, .2], [.3, .5]]),
                         mass=ar([[.1, .2], [.3, .5]]),
                         ret_s=False)
    m = reduce(mul, mats)
    assert m.how_not_conserving() < 1e-7, m.how_not_conserving()
