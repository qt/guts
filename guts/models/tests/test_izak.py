# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
from __future__ import division
import numpy as np
from guts.models import izak

def test_smatrices_mass_stripe():
    import sys
    s1, s2 = izak.smatrices_mass_slice(np.ones((3, 2)))
    det1, det2 = np.linalg.det(s1.to_matrix()), np.linalg.det(s2.to_matrix())
    assert abs(abs(det1) - 1) < 1e-12
    assert abs(abs(det2) - 1) < 1e-12
