# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
from __future__ import division
import numpy as np, numpy.matlib as ml
from numpy import array as ar

from guts.models.dirac_d_wave import *
from guts import misc, sm
from guts.misc import how_different

# These tests only check that functions evaluate and produce sane output.

def test_prepare_hamiltonian():
    dh = prepare_hamiltonian(.5, .1)
    for i in dh:
        if i != 't':
            assert how_different(dh[i], dh[i].H) < 1e-15, \
                   how_different(dh[i], dh[i].H)

def test_s_free():
    dh = prepare_hamiltonian(.2, .4)
    s = s_free(3, 0.2, dh, {'mu_0': .2, 'mu_ac': 1, 'en': 2, 'i_d': .7})
    assert s.how_nonunitary() < 1e-13, s.how_nonunitary()

def test_s_slice():
    ar = np.array
    dh = prepare_hamiltonian(.2, .4)
    pot = {'mu_0': ar([-.2, .1j, 1.5]), 'mu_ac': ar([.8, .1, .2 + .2j])}
    s = s_slice(.4, dh, pot, 1)
    assert s.how_nonunitary() < 1e-13, s.how_nonunitary()

def test_tun_bar():
    dh = prepare_hamiltonian(.2, .4)
    s = tun_bar(.2, 2, dh['mu_0'])
    assert s.how_nonunitary() < 1e-13, s.how_nonunitary()

def test_el_cond():
    dh = prepare_hamiltonian(.2, .4)
    s = s_free(5, 0.2, dh, {'mu_0': .2, 'mu_ac': 1, 'en': 2, 'i_d': .7})
    assert el_cond(s.t, .1, .2, dh).imag == 0

