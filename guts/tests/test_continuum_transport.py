# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
from __future__ import division
import numpy as np, numpy.matlib as ml
from scipy import linalg as la

from guts.continuum_transport import *
from guts import misc, sm, matrix as mat

def test_find_modes():
    """Test find_modes."""
    np.random.seed(5)
    j = mat.make_some_current(4)
    t = find_modes(j)
    assert misc.how_different(mat.sigma_z(4, False), t.H * j * t) < 1e-12

def test_v_to_current():
    """Test hermiticity of the output of v_to_current."""
    np.random.seed(4)
    v = mat.make_gaussian_h(10)
    current = [mat.make_some_current(3), mat.make_some_current(2)]
    t = [find_modes(i) for i in current]
    v1 = v_to_current(v, t)
    assert misc.how_different(v1, v1.H) < 1e-15

def test_smatrix_free():
    """Test unitarity of the output of smatrix_free."""
    np.random.seed(2)
    v = [mat.make_gaussian_h(4), mat.make_gaussian_h(4)]
    assert smatrix_free(v).how_nonunitary() < 1e-12

def test_smatrix_mixing():
    """Test unitarity of the output of smatrix_mixing."""
    np.random.seed(1)
    v = mat.make_gaussian_h(20)
    assert smatrix_mixing(v, 2).how_nonunitary() < 1e-10
