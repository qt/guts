# Copyright 2010 A. R. Akhmerov, C. W. Groth, M. Wimmer.
#
# This file is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution.
from __future__ import division
import numpy as np, numpy.matlib as ml
from scipy import linalg as la, sparse as sp

from guts import sm
from guts.matrix import *
from guts.misc import how_different

# These tests do not yet check that the corresponding matrices are belonging
# to the proper uniform distributions, only the basic properties are tested.

def test_make_gaussian_h():
    """Test symmetry and hermiticity of the output of make_gaussian_h."""
    np.random.seed(1)
    for sym in ['A', 'AI', 'AII', 'AIII', 'BDI', \
                'CII', 'D', 'DIII', 'C', 'CI']:
        h = make_gaussian_h(4, sym)
        assert how_different(h, h.H) < 1e-15
        if sym == 'AI':
            assert abs(np.imag(h)).max() == 0
        elif sym == 'AII':
            assert how_different(np.conj(h),
                                 -isigma_y(2) * h * isigma_y(2))  < 1e-15
        elif sym == 'AIII':
            assert how_different(-h, sigma_z(2) * h * sigma_z(2)) < 1e-15
        elif sym == 'BDI':
            assert abs(np.real(h)).max() == 0
            assert how_different(-h, sigma_z(2) * h * sigma_z(2)) < 1e-15
        elif sym == 'CII':
            tau_z = kron(np.identity(2), sigma_z(1, False))
            assert how_different(np.conj(h),
                                 -isigma_y(2) * h * isigma_y(2))  < 1e-15
            assert how_different(-h, tau_z * h * tau_z) < 1e-15
        elif sym == 'D':
            assert abs(np.real(h)).max() == 0
        elif sym == 'DIII':
            assert abs(np.real(h)).max() == 0
            assert how_different(h, isigma_y(2) * h * isigma_y(2))  < 1e-15
        elif sym == 'C':
            assert how_different(np.conj(h),
                                 isigma_y(2) * h * isigma_y(2)) < 1e-15
        elif sym == 'CI':
            assert abs(np.imag(h)).max() == 0
            assert how_different(h, isigma_y(2) * h * isigma_y(2)) < 1e-15

def test_make_some_current():
    """Test symmetry and absence of chirality of make_some_current."""
    np.random.seed(1)
    for sym in ['A', 'AI', 'D']:
        cur = make_some_current(4, sym)
        vals = la.eigh(cur)[0]
        for i in range(4):
            assert vals[i] < 0
        for i in range(4, 8):
            assert vals[i] > 0
        assert how_different(cur, cur.H)  < 1e-15
        if sym == 'AI':
            assert abs(np.imag(cur)).max() == 0
        elif sym == 'D':
            assert abs(np.real(cur)).max() < 1e-13

def test_make_circular_s():
    """Test symmetry and unitarity of make_circular_s."""
    np.random.seed(1)
    for sym in ['A', 'AI', 'AII', 'AIII', 'BDI', \
                'C', 'CII', 'D', 'DIII', 'C', 'CI']:
        s = make_circular_s(8, sym)
        assert s.how_nonunitary() < 1e-13
        assert not how_different(s.to_matrix(), np.identity(16)) < 1e-15
        s = s.to_matrix()
        assert s.shape == (16, 16)
        # Check e-h symmetry:
        if sym == 'D' or sym == 'DIII' or sym == 'BDI':
            assert abs(np.imag(s)).max() == 0
        elif sym == 'C' or sym == 'CI' or sym == 'CII':
            s_y = kron(np.identity(8), sigma_y(1, False))
            assert s_y.shape == (16, 16)
            assert how_different(s, s_y * np.conj(s) * s_y) < 1e-14

        # Check time-reversal symmetry:
        if sym in ['AI', 'CI', 'BDI']:
            assert how_different(s, s.T) < 1e-15
        elif sym == 'AII' or sym == 'DIII':
            assert how_different(s, -s.T) < 1e-15

        # Check chiral symmetry for AIII and CII:
        if sym == 'AIII':
            assert how_different(s, s.H) < 1e-15

def test_make_circular_s_cond():
    """Test average conductance and its variance of CUE, CRE and CQE.

    These three are chosen to check that random elements of symmetric groups
    are drawn according to Haar measure.
    """
    np.random.seed(1)
    for sym in ['D', 'A', 'C']:
        cond = []
        for i in xrange(300):
            s = make_circular_s(2, sym)
            cond += [s.cond()]
        if sym == 'A':
            assert abs(np.mean(cond) - 1) < 2e-2, np.mean(cond)
            assert abs(np.var(cond) - 1/15) < 2e-2, np.var(cond)
        elif sym == 'D':
            assert abs(np.mean(cond) - 1) < 2e-2, np.mean(cond)
            assert abs(np.var(cond) - 1/9) < 2e-2, np.var(cond)
        elif sym == 'C':
            assert abs(np.mean(cond) - 1) < 2e-2, np.mean(cond)
            assert abs(np.var(cond) - 1/5) < 2e-2, np.var(cond)

def test_make_circular_s_charge():
    """Test if make_circular_s gives correct topological charge."""
    for i in range(10):
        # Checking class D
        s = make_circular_s(4, 'D', 1).to_matrix()
        assert abs(la.det(s) - 1) < 1e-13, la.det(s)
        s = make_circular_s(4, 'D', -1).to_matrix()
        assert abs(la.det(s) + 1) < 1e-13, la.det(s)
        # Checking class DIII
        s = make_circular_s(4, 'DIII', 1).to_matrix()
        assert abs(pf(s) - 1) < 1e-13, pf(s)
        s = make_circular_s(4, 'DIII', -1).to_matrix()
        assert abs(pf(s) + 1) < 1e-13, pf(s)
        s = make_circular_s(6, 'DIII', 1).to_matrix()
        assert abs(pf(s) + 1) < 1e-13, pf(s)
        s = make_circular_s(6, 'DIII', -1).to_matrix()
        assert abs(pf(s) - 1) < 1e-13, pf(s)
        # Checking chiral classes
        s = make_circular_s(4, 'AIII', -1).to_matrix()
        assert sum(la.eigh(s)[0] < 0) == 3, la.eigh(s)
        s = make_circular_s(4, 'BDI', -2).to_matrix()
        assert sum(la.eigh(s)[0] < 0) == 2, la.eigh(s)
        s = make_circular_s(4, 'CII', -1).to_matrix()
        assert sum(la.eigh(s)[0] < 0) == 2, la.eigh(s)


def test_make_circular_m():
    """Test symmetry and unitarity of make_circular_s."""
    np.random.seed(1)
    for sym in ['A', 'D']:
        m = make_circular_m(2, sym)
        assert m.how_not_conserving() < 1e-5, m.how_not_conserving()
        m = m.to_matrix()
        if sym == 'D':
            assert abs(np.imag(m)).max() == 0
