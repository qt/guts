Momentum space discretization
=============================

If you find the implementation of momentum space discretization useful, we ask
you to cite

  J. K. Asboth, A. R. Akhmerov, M. V. Medvedyeva, C. W. J. Beenakker Phys. Rev. B 83, 134519 (2011)

where this implementation was used.
